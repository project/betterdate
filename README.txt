
OVERVIEW

In Drupal 4.7, 'date' type form fields cannot distinguish between required and
non-required fields. In both cases, a user must enter a date, even if the field
has been designated not required (e.g. a birthday profile field was set up as
optional, but the user must still enter a date).

This module, once enabled, will silently find all date fields and exchange them
with 'better date' fields. Better date fields allow users to enter null dates
for non-required fields. For required fields, behavior remains the same. For the
purposes of theming better dates appear as dates.

INSTALLATION

Just enable in admin/modules and you're ready to go.

AUTHOR
Mark Fredrickson
mark@advantagelabs.com
